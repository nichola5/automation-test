class locator(object):

    signin                              = "//a[@class='login']"
    email                               = "//input[@id='email']"
    password                            = "//input[@id='passwd']"
    submit                              = "//p[@class='submit']//span[1]"
    logout                              = "//a[@class='logout']"

    dress                               = "/html[1]/body[1]/div[1]/div[1]/header[1]/div[3]/div[1]/div[1]/div[6]/ul[1]/li[2]/a[1]"
    category                            = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[2]/div[3]/div[2]"
    printed_dress                       = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[2]/ul[1]/li[2]/div[1]/div[2]/h5[1]/a[1]"
    size                                = "//select[@id='group_1']"
    add_to_cart                         = "//span[contains(text(),'Add to cart')]"
    layer_cart                          = "//div[@id='layer_cart']"
    sub_layer_cart                      = "//div[@class='layer_cart_product_info']"

    checkbox                            = "//input[@id='cgv']"

    popup                               = "/html[1]/body[1]/div[1]/div[1]/header[1]/div[3]/div[1]/div[1]/div[4]/div[1]/div[2]"
    proceed_checkout                    = "//span[contains(text(),'Proceed to checkout')]"

    proceed_1                           = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[1]/p[2]/a[1]/span[1]"
    proceed_2                           = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[1]/form[1]/p[1]/button[1]/span[1]"
    proceed_3                           = "//button[@name='processCarrier']//span[contains(text(),'Proceed to checkout')]"

    payment_tab                         = "//span[contains(text(),'Payment')]"

#validation

    product_title                       = "//span[@id='layer_cart_product_title']"
    product_attribute                   = "//span[@id='layer_cart_product_attributes']"
    product_quantity                    = "//span[@id='layer_cart_product_quantity']"
    product_price                       = "//span[@id='layer_cart_product_price']"
    total_price                         = "//span[@id='total_price']"
    validating_address                  = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[3]/div[1]/ul[1]/li[4]"
