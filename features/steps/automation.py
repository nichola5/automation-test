from behave import*
from locator import*
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from time import sleep
from database import*
from hamcrest import*

@given(u'automation scenario - opening automation practice')
def step_impl(context):
    context.browser.get(database.userdata["url"])


@when(u'automation scenario - sign in user')
def step_impl(context):
    context.browser.find_element(By.XPATH,locator.signin).click()
    sleep(3)
    context.browser.find_element(By.XPATH,locator.email).send_keys(database.userdata["username"])
    sleep(1)
    context.browser.find_element(By.XPATH,locator.password).send_keys(database.userdata["password"])
    context.browser.find_element(By.XPATH,locator.submit).click()

@then(u'automation scenario - go to dress section')
def step_impl(context):
    context.browser.find_element(By.XPATH,locator.dress).click()
    sleep(2)


@then(u'automation scenario - pick printed dress')
def step_impl(context):
    context.browser.find_element(By.XPATH,locator.printed_dress).click()
    sleep(2)

@then(u'automation scenario - select size and add to cart')
def step_impl(context):
    context.browser.find_element(By.XPATH,locator.size).click()
    context.browser.find_element(By.XPATH,locator.size).send_keys("M")
    context.browser.find_element(By.XPATH,locator.size).click()
    sleep(1)
    context.browser.find_element(By.XPATH,locator.add_to_cart).click()
    sleep(2)

@then(u'automation scenario - validating product name')
def step_impl(context):
    #context.browser.find_element(By.XPATH,locator.layer_cart).is_displayed()
    context.browser.find_element(By.XPATH,locator.product_title).is_displayed()
    prod = context.browser.find_elements(By.XPATH,locator.product_title)
    product = prod[0].text
    assert_that(product, contains_string(database.userdata["product"]))
    sleep(2)

@then(u'automation scenario - validating color, and size')
def step_impl(context):
    context.browser.find_element(By.XPATH,locator.product_attribute).is_displayed()
    att = context.browser.find_elements(By.XPATH,locator.product_attribute)
    attribute = att[0].text
    assert_that(attribute, contains_string(database.userdata["attribute"]))
    sleep(1)

@then(u'automation scenario - validating quantity')
def step_impl(context):
    context.browser.find_element(By.XPATH,locator.product_quantity).is_displayed()
    quant = context.browser.find_elements(By.XPATH,locator.product_quantity)
    quantity = quant[0].text
    assert_that(quantity, contains_string(database.userdata["quantity"]))
    sleep(1)

@then(u'automation scenario - validating product price')
def step_impl(context):
    context.browser.find_element(By.XPATH,locator.product_price).is_displayed()
    pricex = context.browser.find_elements(By.XPATH,locator.product_price)
    price = pricex[0].text
    assert_that(price, contains_string(database.userdata["price"]))
    sleep(1)

@then(u'automation scenario - proceed')
def step_impl(context):
    context.browser.find_element(By.XPATH,locator.proceed_checkout).click()
    sleep(3)

@then(u'automation scenario - validating total product price')
def step_impl(context):
    context.browser.find_element(By.XPATH,locator.total_price).is_displayed()
    total = context.browser.find_elements(By.XPATH,locator.total_price)
    total_price = total[0].text
    assert_that(total_price, contains_string(database.userdata["total_price"]))

@then(u'automation scenario - validating address')
def step_impl(context):
    context.browser.find_element(By.XPATH,locator.validating_address).is_displayed()
    add = context.browser.find_elements(By.XPATH,locator.validating_address)
    address = add[0].text
    assert_that(address, contains_string(database.userdata["address"]))
    sleep(2)
    context.browser.find_element(By.XPATH,locator.proceed_1).click()
    sleep(1)


@then(u'automation scenario - tick next day shipment option')
def step_impl(context):
    context.browser.find_element(By.XPATH,locator.proceed_2).click()
    sleep(2)
    context.browser.find_element(By.XPATH,locator.checkbox).click()
    sleep(2)


@then(u'automation scenario - proceed until payment tab')
def step_impl(context):
    context.browser.find_element(By.XPATH,locator.proceed_3).click()
    sleep(2)
    context.browser.find_element(By.XPATH,locator.payment_tab).is_displayed()


@Then(u'automation scenario - without paying, sign out')
def step_impl(context):
    context.browser.find_element(By.XPATH,locator.logout).click()
    sleep(3)
