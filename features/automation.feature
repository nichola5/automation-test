Feature: automation scenario
Scenario: automation scenario
      Given automation scenario - opening automation practice
      When  automation scenario - sign in user
      Then  automation scenario - go to dress section
      Then  automation scenario - pick printed dress
      Then  automation scenario - select size and add to cart
      Then  automation scenario - validating product name
      Then  automation scenario - validating color, and size
      Then  automation scenario - validating quantity
      Then  automation scenario - validating product price
      Then  automation scenario - proceed
      Then  automation scenario - validating total product price
      Then  automation scenario - validating address
      Then  automation scenario - tick next day shipment option
      Then  automation scenario - proceed until payment tab
      Then  automation scenario - without paying, sign out
