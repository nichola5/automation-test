DESCRIPTION

this is automation test using behave BDD


REQUIREMENTS 

- behave == 1.2.6
- configparser == 3.7.4
- pyhamcrest == 1.9.0
- selenium == 3.141.0


HOW TO RUN 

- go to automation-test-master/features/
- execute "behave automation.features"